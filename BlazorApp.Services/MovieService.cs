﻿using BlazorApp.Models;
using BlazorApp.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace BlazorApp.Services
{
    public class MovieService : IMovies
    {
        public List<Movie> _movies { get; set; }

        public MovieService()
        {
            _movies = new List<Movie>()
            {
                new Movie()
                {
                    Id = 1,
                    Title = "Back To The Future",
                    Year = 1994,
                    Description = "Marty and the Doc goes to the past.",
                    Poster = "URL of th emovie poster"
                },
                 new Movie()
                {
                    Id = 2,
                    Title = "Toy Story",
                    Year = 1994,
                    Description = "A Toys movie.",
                    Poster = "URL of th emovie poster"
                },
                  new Movie()
                {
                    Id = 3,
                    Title = "Pirates of the Caribbean",
                    Year = 2004,
                    Description = "A Pirates movie.",
                    Poster = "URL of th emovie poster"
                }
            };
        }

        public List<Movie> GetMovies()
        {
            return _movies
                .OrderBy(mov => mov.Year)
                .ToList();
        }

        public Movie GetMovieById(int id)
        {
            var movie = _movies.FirstOrDefault(mov => mov.Id == id);
            return movie;
        }

    }
}
