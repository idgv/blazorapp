﻿using System;

namespace BlazorApp.Models
{
    public partial class Movie
    {
        public int Id { get; set; }
        public string ImdbCode { get; set; }
        public string Url { get; set; }
        public string Title { get; set; }
        public string TitleLong { get; set; }
        public string Description { get; set; }
        public int Year { get; set; }
        public string Poster { get; set; }
        public float Rating { get; set; }
        public int Runtime { get; set; }
        public string Language { get; set; }
        public string MpaRating { get; set; }
        public string Hash { get; set; }
        public string MagnetLink { get; set; }
       

    }
}
