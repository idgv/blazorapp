﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BlazorApp.Models;
using BlazorApp.Services.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace BlazorApp.Server.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MovieController : Controller
    {
        private IMovies _movieService { get; set; }
        public MovieController(IMovies movieService)
        {
            _movieService = movieService;
        }

        //Get api/Movie
        [HttpGet]
        public List<Movie> Get()
        {
            return _movieService.GetMovies();
        }

        //GET api/movie/{id}
        [HttpGet("{id}")]
        public Movie GetMovieById(int id)
        {
            return _movieService.GetMovieById(id);
        }


    }
}